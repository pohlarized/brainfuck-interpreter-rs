use std::collections::HashMap;
use std::fs;
use std::io::{self, Read, Write};

const DATA_SIZE_BYTES: usize = 30_000;

#[derive(Debug)]
enum RuntimeError {
    NoMatchingOpenBracket(usize),
    NoMatchingCloseBacket(usize),
    DataPointerOutOfBounds(usize),
    DataByteOutOfBounds(usize),
    MissingInput(usize),
    OutputWriteFailed(io::Error),
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum Instruction {
    IncrementDataPointer,
    DecrementDataPointer,
    IncrementDataByte,
    DecrementDataByte,
    OutputDataByte,
    GetInputByte,
    JumpForwardsIfZero,
    JumpBackwardsIfNotZero,
}

struct Process {
    instructions: Vec<Instruction>,
    jump_table: HashMap<usize, usize>,
    instruction_pointer: usize,
    data: [u8; DATA_SIZE_BYTES],
    data_pointer: usize,
}

impl Process {
    pub fn new(instructions: Vec<Instruction>, jump_table: HashMap<usize, usize>) -> Self {
        Self {
            instructions,
            jump_table,
            instruction_pointer: 0,
            data: [0; DATA_SIZE_BYTES],
            data_pointer: 0,
        }
    }

    fn execute(&mut self, instruction: Instruction) -> Result<(), RuntimeError> {
        match instruction {
            Instruction::IncrementDataPointer => match self.data_pointer.checked_add(1) {
                Some(result) => self.data_pointer = result,
                None => {
                    return Err(RuntimeError::DataPointerOutOfBounds(
                        self.instruction_pointer,
                    ))
                }
            },
            Instruction::DecrementDataPointer => match self.data_pointer.checked_sub(1) {
                Some(result) => self.data_pointer = result,
                None => {
                    return Err(RuntimeError::DataPointerOutOfBounds(
                        self.instruction_pointer,
                    ))
                }
            },
            Instruction::IncrementDataByte => match self.data.get(self.data_pointer) {
                Some(result) => match result.checked_add(1) {
                    Some(result) => self.data[self.data_pointer] = result,
                    None => {
                        return Err(RuntimeError::DataByteOutOfBounds(self.instruction_pointer))
                    }
                },
                None => {
                    return Err(RuntimeError::DataPointerOutOfBounds(
                        self.instruction_pointer,
                    ))
                }
            },
            Instruction::DecrementDataByte => match self.data.get(self.data_pointer) {
                Some(result) => match result.checked_sub(1) {
                    Some(result) => self.data[self.data_pointer] = result,
                    None => {
                        return Err(RuntimeError::DataByteOutOfBounds(self.instruction_pointer))
                    }
                },
                None => {
                    return Err(RuntimeError::DataPointerOutOfBounds(
                        self.instruction_pointer,
                    ))
                }
            },
            Instruction::GetInputByte => {
                let input = io::stdin().bytes().next().and_then(|result| result.ok());
                if let Some(byte) = input {
                    self.data[self.data_pointer] = byte;
                } else {
                    return Err(RuntimeError::MissingInput(self.instruction_pointer));
                };
            }
            Instruction::OutputDataByte => {
                let mut stdout = io::stdout().lock();
                let byte = match self.data.get(self.data_pointer) {
                    Some(byte) => *byte,
                    None => return Err(RuntimeError::DataPointerOutOfBounds(self.instruction_pointer))
                };
                if let Err(e) = stdout.write_all(&[byte]) {
                    return Err(RuntimeError::OutputWriteFailed(e));
                }
            }
            Instruction::JumpBackwardsIfNotZero => {
                if self.data[self.data_pointer] != 0 {
                    self.instruction_pointer =
                        // Since the jump table is built from the given instructions it's
                        // impossible the the get returns an error
                        *self.jump_table.get(&self.instruction_pointer).unwrap();
                }
            }
            Instruction::JumpForwardsIfZero => {
                if self.data[self.data_pointer] == 0 {
                    self.instruction_pointer =
                        // Since the jump table is built from the given instructions it's
                        // impossible the the get returns an error
                        *self.jump_table.get(&self.instruction_pointer).unwrap();
                }
            }
        };
        Ok(())
    }

    pub fn run(&mut self) -> Result<(), RuntimeError> {
        let no_instructions = self.instructions.len();
        while self.instruction_pointer < no_instructions {
            // self.show();  // this is only for debugging
            let instruction = self.instructions[self.instruction_pointer];
            self.execute(instruction)?;
            self.instruction_pointer += 1;
        }
        Ok(())
    }

    #[allow(dead_code)]
    pub fn show(&self) {
        // This is just a debug function that i want to keep
        println!("#### Instruction ptr: {} ####", self.instruction_pointer);
        println!(
            "Current Instruction: {:?}",
            self.instructions[self.instruction_pointer]
        );
        println!("Data pointer: {}", self.data_pointer);
        println!("Current data byte: {}", self.data[self.data_pointer]);
    }
}

fn tokenize(code: String) -> Vec<Instruction> {
    code.chars()
        .filter_map(|character| match character {
            '>' => Some(Instruction::IncrementDataPointer),
            '<' => Some(Instruction::DecrementDataPointer),
            '+' => Some(Instruction::IncrementDataByte),
            '-' => Some(Instruction::DecrementDataByte),
            '.' => Some(Instruction::OutputDataByte),
            ',' => Some(Instruction::GetInputByte),
            '[' => Some(Instruction::JumpForwardsIfZero),
            ']' => Some(Instruction::JumpBackwardsIfNotZero),
            _ => None,
        })
        .collect()
}

fn create_jump_table(instructions: &[Instruction]) -> Result<HashMap<usize, usize>, RuntimeError> {
    let mut jump_table = HashMap::new();
    let mut left_brackets = Vec::new();
    for (index, instruction) in instructions.iter().enumerate() {
        match instruction {
            Instruction::JumpForwardsIfZero => left_brackets.push(index),
            Instruction::JumpBackwardsIfNotZero => {
                if let Some(matching_opener) = left_brackets.pop() {
                    jump_table.insert(matching_opener, index);
                    jump_table.insert(index, matching_opener);
                } else {
                    return Err(RuntimeError::NoMatchingOpenBracket(index));
                }
            }
            _ => (),
        }
    }
    if let Some(pos) = left_brackets.pop() {
        return Err(RuntimeError::NoMatchingCloseBacket(pos));
    }
    Ok(jump_table)
}

fn main() {
    let filename = "./script.bf";
    let source_code = fs::read_to_string(filename).unwrap();
    let instructions = tokenize(source_code);
    let jump_table = create_jump_table(&instructions).unwrap();
    println!("{:?}", instructions);
    let mut interpreter = Process::new(instructions, jump_table);
    interpreter.run().unwrap();
}
